package com.andymur.pg.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleRestController {
	@RequestMapping("/")
	public String root() {
		return "Hi!";
	}
}
